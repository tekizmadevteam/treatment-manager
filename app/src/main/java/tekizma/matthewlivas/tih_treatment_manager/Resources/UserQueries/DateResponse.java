package tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries;

import java.util.Calendar;

/**
 * This class is a concrete implementation of QueryResponse and represents a user's response to a
 * query about a Date.
 * Created by MatthewLivas on 5/2/2016.
 */
public class DateResponse extends QueryResponse {

    public DateResponse (String question_id, String question) {
        super(question_id, question, QueryType.DATE);
    }

    @Override
    /**
     * Expecting a Calendar or subclass of Calendar object which has been set to the date asked for
     * in the corresponding question. Will also accept a String in the form "XX/XX/XXXX" where the
     * first block is the month and the second block is the day.
     * Throws an illegal argument exception if the parameter is not a Calendar. subclass of
     * Calendar, or a properly formatted String.
     */
    public void setUserResponse(Object obj) {
        if(obj instanceof Calendar) {
            Calendar cal = (Calendar) obj;

            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            String formattedMonth = (month < 10) ? "0" + String.valueOf(month) : String.valueOf(month);
            String formattedDay = (day < 10) ? "0" + String.valueOf(day) : String.valueOf(day);

            this.user_response = formattedMonth+ "/" + formattedDay + "/" + cal.get(Calendar.YEAR);

        } else if (obj instanceof String) {
            String input = (String) obj;
            if(input.matches("[0|1]\\d/[0-3]\\d/\\d\\d\\d\\d")) {
                user_response = input;
            } else {
                throw new IllegalArgumentException("Input string is improperly formatted. " + obj.toString());
            }
        } else {
            throw new IllegalArgumentException("Improper input for setting a DateResponse user response." + obj.toString());
        }
    }
}
