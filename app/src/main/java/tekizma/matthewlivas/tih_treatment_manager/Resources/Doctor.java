package tekizma.matthewlivas.tih_treatment_manager.Resources;

/**
 * This class represents a Doctor, as stored in the Tekizma Integrated Health System (TIH).
 * This class is immutable.
 * Created by MatthewLivas on 4/27/2016.
 */
public class Doctor {

    /** This field represents the Doctor's unique ID in TIH */
    public final String doctor_id;
    /** This field represents the Doctor's provider ID (Associated practice, hospital, etc.) */
    public final String provider_id;
    /** Doctor first name */
    public final String dr_first_name;
    /** Doctor last name */
    public final String dr_last_name;
    /** Doctor's primary email */
    public final String dr_email;

    /**
     * Basic full constructor
     * @param doctor_id unique Doctor ID in TIH
     * @param provider_id ID associated with the Doctor's hospital, clinic, practice, etc.
     * @param firstName Doctor first name
     * @param lastName Doctor last name
     * @param email Doctor email address
     */
    public Doctor(String doctor_id, String provider_id, String firstName, String lastName, String email) {
        this.doctor_id = doctor_id;
        this.provider_id = provider_id;
        this.dr_first_name = firstName;
        this.dr_last_name = lastName;
        this.dr_email = email;
    }
}
