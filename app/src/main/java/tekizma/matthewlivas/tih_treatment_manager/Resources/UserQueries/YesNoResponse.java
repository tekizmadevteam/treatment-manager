package tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries;

/**
 * This class is a concrete implementation of QueryResponse and represents a Yes or No response from
 * the user.
 * Created by MatthewLivas on 5/2/2016.
 */
public class YesNoResponse extends QueryResponse {

    public YesNoResponse (String question_id, String question) {
        super(question_id, question, QueryType.YES_NO);
    }

    @Override
    /**
     * Expects a boolean or a String saying either "yes" or "no" ignoring capitalization. This method
     * throws an IllegalArgumentException if the parameter is invalid for setting this response type.
     */
    public void setUserResponse(Object obj) {
        if (obj instanceof Boolean) {
            if ((Boolean) obj) {
                user_response = "Yes";
            } else {
                user_response = "No";
            }
        } else if (obj instanceof String) {
            String response = (String) obj;

            if (response.equalsIgnoreCase("Yes")) {
                user_response = "Yes";
            } else if (response.equalsIgnoreCase("No")) {
                user_response = "No";
            } else {
                throw new IllegalArgumentException("String does not represent either Yes or No. " + obj.toString());
            }
        //} else if (obj == (boolean) obj) {        TODO: Commented out for purposes of testing UI
        //    if ((boolean) obj) {
        //        user_response = "Yes";
        //    } else {
        //        user_response = "No";
        //    }
        } else {
            throw new IllegalArgumentException("Improper input for setting a YesNoResponse user response. " + obj.toString());
        }
    }
}
