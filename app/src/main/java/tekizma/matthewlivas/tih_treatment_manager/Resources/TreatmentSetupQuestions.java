package tekizma.matthewlivas.tih_treatment_manager.Resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.QueryResponse;

/**
 * Class to bundle together QueryResponses together to form a full set of unique questions.
 * Created by MatthewLivas on 5/2/2016.
 */
public class TreatmentSetupQuestions {

    /**
     * TIH System generated treatment plan id
     */
    public final String plan_id;
    /**
     * TIH System generated doctor id
     */
    public final String doctor_id;
    /**
     * TIH System generated patient id
     */
    public final String patient_id;
    /**
     * Mapping of question_id to QueryResponse
     */
    protected TreeMap<String, QueryResponse> questions = new TreeMap<String, QueryResponse>();

    /**
     * Nate, your wish has been granted
     * The size of the value set
     * @return size of the value set
     */
    public int valueSize() {
        return questions.size();
    }

    public TreatmentSetupQuestions(String planID, String doctorID, String patientID) {
        this.plan_id = planID;
        this.doctor_id = doctorID;
        this.patient_id = patientID;
    }

    /**
     * Adds a new QueryResponse to this bundle of questions if not already contained.
     * @param q QueryResponse to add
     * @return True if not already contained, false otherwise.
     */
    public boolean addQueryResponse(QueryResponse q) {
        if(questions.containsKey(q.question_id)) {
            return false;
        } else {
            questions.put(q.question_id, q);
            return true;
        }
    }

    /**
     * Remove a QueryResponse from this bundle of questions
     * @param questionID ID of the QueryResponse to be removed
     * @return The QueryResponse that was removed
     */
    public QueryResponse removeQueryResponse(String questionID) {
        return questions.remove(questionID);
    }

    /**
     * Checks if a QueryResponse is contained.
     * @param questionID ID of question to search for
     * @return True if the specified key is contained within this bundle of questions.
     */
    public boolean containsQueryResponse(String questionID) {
        return questions.containsKey(questionID);
    }

    /**
     * Return the specified QueryResponse, if contained.
     * @param questionID ID of the question to return.
     * @return The QueryResponse with the specified ID.
     */
    public QueryResponse getQueryResponse(String questionID) {
        return questions.get(questionID);
    }

    /**
     * Update a contained QueryResponse with the specified user response.
     * @param questionID ID of the QueryResponse to update
     * @param userResponse formatted userResponse to update the specified QueryResponse with
     * @return True if the QueryResponse with the specified ID is contained and successfully updated
     * False otherwise.
     */
    public boolean setUserResponse(String questionID, Object userResponse) {
        if(questions.containsKey(questionID)) {
            QueryResponse response = questions.get(questionID);
                response.setUserResponse(userResponse);
                return true;
        }
        return false;
    }

    public int getNumQuestions() {
        return questions.size();
    }

    /**
     * Get the contained QueryResponses as a basic array. No guaranteed order, will fix if needed.
     * @return basic array of the contained QueryResponses
     */
    public QueryResponse[] getQuestionArray() {
        ArrayList<QueryResponse> outList = new ArrayList<QueryResponse>();

        Iterator<Map.Entry<String, QueryResponse>> iterator = questions.entrySet ().iterator();

        Map.Entry<String, QueryResponse> current;

        while(iterator.hasNext()) {
            current = iterator.next();

            outList.add(current.getValue());
        }

        QueryResponse[] returnedList = new QueryResponse[outList.size()];

        return outList.toArray(returnedList);
    }


    public String toString() {
        return "\nPlanID: {" + plan_id + "}\nDoctorID: {" + doctor_id +"}\nPatientID: {"
                + patient_id + "}\nQuestionsTreeMap: {" + questions.toString() + "}.";
    }
}
