package tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries;

/**
 * This class is a concrete implementation of QueryResponse representing a Numeric reponse from the
 * user.
 * Created by MatthewLivas on 5/2/2016.
 */
public class NumberResponse extends QueryResponse{

    public NumberResponse (String question_id, String question) {
        super(question_id, question, QueryType.NUMBER);
    }

    @Override
    /**
     * Expects an int, an Integer, or a String representing an Integer. This method throws an
     * IllegalArgumentException if the parameter is invalid for setting this response type.
     */
    public void setUserResponse(Object obj) {
        Integer number;

        if(obj instanceof String) {
            try {
                number = Integer.valueOf((String) obj);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("String does not properly represent an integer." + obj.toString());
            }
        } else if(obj instanceof Integer) {
            number = (Integer) obj;
        //} else if( obj == (int) obj){     TODO; Commented out for UI testing purposes
        //    number = (int) obj;
        } else {
            throw new IllegalArgumentException("Improper input for setting a TimeResponse user response." + obj.toString());
        }

        this.user_response = String.valueOf(number);

    }


}
