package tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries;

import java.util.Calendar;

/**
 * This class is a concrete implementation of QueryResponse and  represents a user's response to a
 * query about a Time.
 * Created by MatthewLivas on 5/2/2016.
 */
public class TimeResponse extends QueryResponse {

    public TimeResponse (String question_id, String question) {
        super(question_id, question, QueryType.TIME);
    }

    @Override
    /**
     * Expecting a Calendar or subclass of Calendar object which has been set to the time asked for
     * in the corresponding question. Will also accept a String formatted as XX:XX where the first
     * block is the Hour in a 24 hour clock and the second block is the Minute. Throws an illegal
     * argument exception if the parameter is not a Calendar, subclass of Calendar, or a properly
     * formatted String.
     */
    public void setUserResponse(Object obj) {
        if(obj instanceof Calendar) {
            Calendar cal = (Calendar) obj;

            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);

            String formattedHour = (hour < 10) ? "0" + String.valueOf(hour) : String.valueOf(hour);
            String formattedMin = (minute < 10) ? "0" + String.valueOf(minute) : String.valueOf(minute);

            this.user_response = formattedHour + ":" + formattedMin;

        } else if(obj instanceof String) {
            String input = (String) obj;

            if(input.matches("[0-2]\\d:[0-5]\\d")) {
                user_response = input;
            } else {
                throw new IllegalArgumentException("Improperly String is improperly formatted. " + obj.toString());
            }

        } else {
            throw new IllegalArgumentException("Improper input for setting a TimeResponse user response." + obj.toString());
        }
    }

}
