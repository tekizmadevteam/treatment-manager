package tekizma.matthewlivas.tih_treatment_manager.Resources;

/**
 * This class represents a Patient as stored in the Tekizma Integrated Health System (TIH).
 * This class is currently immutable.
 * Created by MatthewLivas on 4/27/2016.
 */
public class Patient {
    /** Patient's unique ID within TIH */
    public final String patient_id;
    /** Patient's associated Doctor's ID within TIH */
    public final String doctor_id;
    /** Patient's primary email */
    public final String patient_email;
    /** Patient's password. Should not be stored on the phone or in plainText */
    private final String password;
    /** Patient's first name */
    public final String first_name;
    /** Patient's last name */
    public final String last_name;
    /** Patient's primary phone number */
    public final String phone_number;
    /** Patient's DOB [Month]/[Day]/[Year] */
    public final String date_of_birth;

    /**
     * Basic full constructor
     * @param patient_id Patient's unique ID within TIH
     * @param doctor_id Patient's associated Doctor's ID within TIH
     * @param patient_email Patient's email
     * @param password Patient's password
     * @param first_name Patient's first name
     * @param last_name Patient's last name
     * @param phone_number Patient's phone number XXX-XXX-XXXX
     * @param date_of_birth Patient's DOB [Month]/[Day]/[Year]
     */
    public Patient(String patient_id, String doctor_id, String patient_email, String password,
                   String first_name, String last_name, String phone_number, String date_of_birth) {
        this.patient_id = patient_id;
        this.doctor_id = doctor_id;
        this.patient_email = patient_email;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_number = phone_number;
        this.date_of_birth = date_of_birth;
    }

    /**
     * Basic full constructor
     * @param patient_id Patient's unique ID within TIH
     * @param doctor_id Patient's associated Doctor's ID within TIH
     * @param patient_email Patient's email
     * @param password Patient's password
     * @param first_name Patient's first name
     * @param last_name Patient's last name
     * @param phone_number Patient's phone number XXX-XXX-XXXX
     * @param month Patient's month of birth
     * @param day Patient's day of birth
     * @param year Patient's year of birth
     */
    public Patient(String patient_id, String doctor_id, String patient_email, String password,
                   String first_name, String last_name, String phone_number, int month, int day,
                   int year) {
        this(patient_id, doctor_id, patient_email, password, first_name, last_name,
                phone_number, (month + "/" + day + "/" + year));
    }

    /**
     * @return patient password
     */
    public String getPassword() {
        return password;
    }
}
