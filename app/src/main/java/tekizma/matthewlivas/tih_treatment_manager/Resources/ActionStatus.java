package tekizma.matthewlivas.tih_treatment_manager.Resources;

/**
 * Created by MatthewLivas on 4/11/2016.
 *
 * Pending: User has not interacted with the action yet
 * Completed: User has said "Yes"
 * Not_Completed: User said "No"
 * Timed_Out: User said "Snooze" too many times, or the time limit to respond has passed
 */
public enum ActionStatus {
    PENDING, COMPLETED, TIMED_OUT, NOT_COMPLETED;

    @Override
    public String toString() throws IllegalArgumentException {
        switch (this) {
            case PENDING: return "Pending";
            case COMPLETED: return "Completed";
            case TIMED_OUT: return "Timed_Out";
            case NOT_COMPLETED: return "Not_Completed";
            default: throw new IllegalArgumentException("Invalid Enum Option");
        }
    }

    public static ActionStatus getStatusFromString(String s) {
        switch (s) {
            case "Pending": return PENDING;
            case "Completed": return COMPLETED;
            case "Timed_Out": return TIMED_OUT;
            case "Not_Completed": return NOT_COMPLETED;
            default: throw new IllegalArgumentException(s + " is not a valid ActionStatus option");
        }
    }
}
