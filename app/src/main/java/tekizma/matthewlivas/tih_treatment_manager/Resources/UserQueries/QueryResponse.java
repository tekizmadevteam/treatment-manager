package tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries;

import org.json.JSONObject;

import tekizma.matthewlivas.tih_treatment_manager.ServerInteraction.JSONTranslator;

/**
 * This class represents the User response to a query posed by the app.
 * Created by MatthewLivas on 5/2/2016.
 */
public abstract class QueryResponse {

    /**
     * Holds the TIH System generated question ID.
     */
    public final String question_id;

    /**
     * Holds the question asked in the query.
     */
    public final String question;
    /**
     * Holds the type of query.
     */
    public final QueryType query_type;
    /**
     * Holds the user's response to the query. Initially the empty string.
     */
    protected String user_response = "";

    /**
     * Basic private constructor. Sets user response to the empty string.
     * @param question_id TIH System generated question ID
     * @param question Question asked to user
     * @param query_type Type of response expected
     */
    public QueryResponse(String question_id, String question, QueryType query_type) {
        this.question_id = question_id;
        this.question = question;
        this.query_type = query_type;
    }

    public String toString() {
        return "{QuestionID: {" + question_id + "}\nQuestion{" + question + "}\nQueryType{"
                + query_type.toString() +"}\nUserResponse: {" + user_response +"}}";
    }

    /**
     * User's response to the question. Initially the empty string. Will be in a proper format, such
     * as "[Month]/[Day]/[Year]" for a Date or "[Hour]:[Minute]" in "Military" time for a Time.
     * @return User response.
     */
    public String getUserResponse() {
        return user_response;
    }

    @Override
    /**
     * This method determines equality of a QueryResponse based on its question_id and query_type
     */
    public boolean equals(Object o) {
        if(o instanceof QueryResponse) {
            QueryResponse other = (QueryResponse) o;
            if(this.question_id.equals(other.question_id) && this.query_type.equals(other.query_type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set the user's response to the question.
     * @param obj Object representing the user's response.
     */
    public abstract void setUserResponse(Object obj);

}
