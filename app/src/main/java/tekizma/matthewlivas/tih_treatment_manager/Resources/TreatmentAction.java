package tekizma.matthewlivas.tih_treatment_manager.Resources;

import java.util.GregorianCalendar;

/**
 * This class will be used to store a single instance of a Treatment Plan Action. It includes the
 * information about the plan it is a part of, the action to be accomplished for this action, the
 * date and time the action should occur, and a way to store any associated observation values, such
 * as a blood sugar reading.
 * Created by MatthewLivas on 4/11/2016.
 */
public class TreatmentAction {

    /**String to hold the ID for the treatment plan this action is a part of*/
    private final String treatment_plan_id;
    /**String to hold the patient's ID*/
    private final String patient_id;
    /**String to hold the action to be preformed*/
    private String action;
    /**Calendar object to hold the date and time of the action. Can be any subclass of Calendar May
     * just want to use a Date object, not sure yet*/
    private GregorianCalendar dateTime;
    /**Enumeration to denote the action's status with regard to user interaction. Guaranteed not null*/
    private ActionStatus status;
    /**Place to store any observations. May be Null or the empty String "" */
    private String observationValue;

    /*Full basic constructor*/
    public TreatmentAction(String treatment_plan_id, String patient_id, String action,
                              GregorianCalendar dateTime, ActionStatus status, String observation) {
        this.treatment_plan_id = treatment_plan_id;
        this.patient_id = patient_id;
        this.action = action;
        this.dateTime = dateTime;
        this.status = status;
        this.observationValue = observation;
    }

    /**
     * Constructor which sets status to Pending and observationValue to the empty string ("")
     * @param treatment_plan_id String to hold the ID for the treatment plan this action is a part of
     * @param patient_id String to hold the patient's ID
     * @param action String to hold the action to be preformed
     * @param dateTime Calendar object to hold the date and time of the action. Can be any subclass
     *                 of Calendar
     */
    public TreatmentAction(String treatment_plan_id, String patient_id, String action, GregorianCalendar dateTime) {
        this(treatment_plan_id, patient_id, action, dateTime, ActionStatus.PENDING, "");
    }

    /**
     * Constructor which sets status to PENDING
     * @param treatment_plan_id String to hold the ID for the treatment plan this action is a part of
     * @param patient_id String to hold the patient's ID
     * @param action String to hold the action to be preformed
     * @param dateTime Calendar object to hold the date and time of the action. Can be any subclass
     *                 of Calendar
     * @param observation String to hold any values to be communicated back to the TIH Server, such
     *                    as Blood Sugar Level
     */
    public TreatmentAction(String treatment_plan_id, String patient_id, String action, GregorianCalendar dateTime,
                           String observation) {
        this(treatment_plan_id, patient_id, action, dateTime, ActionStatus.PENDING, observation);
    }

    /**
     * @return ID for associated treatment plan
     */
    public String getTreatmentPlanID() {
        return treatment_plan_id;
    }

    /**
     * @return ID for the patient whose TreatmentPlan this TreatmentAction is a part of
     */
    public String getPatientID() {
        return patient_id;
    }

    /**
     * @return Task associated with this TreatmentAction
     */
    public String getAction() {
        return action;
    }

    /**
     * @param newTask Task to preform for this TreatmentAction
     */
    public void setAction(String newTask) {
        this.action = newTask;
    }

    /**
     * @return The GergorianCalendar which contains the date and time information for when this
     * TreatmentAction should be preformed
     */
    public GregorianCalendar getDateTime() {
        return dateTime;
    }

    /**
     * @param newDateTime A GregorianCalendar representing the new date and time at which this
     *                    TreatmentAction should be preformed
     */
    public void setDateTime(GregorianCalendar newDateTime) {
        this.dateTime = newDateTime;
    }

    /**
     * @return The status of the TreatmentAction
     */
    public ActionStatus getStatus() {
        return status;
    }

    /**
     * @param newStatus Updates the status field for this TreatmentAction
     * @throws IllegalArgumentException Thrown if newStatus is null
     */
    public void setStatus(ActionStatus newStatus) throws IllegalArgumentException {
        if(null == newStatus) {
            throw new IllegalArgumentException("newStatus cannot be null");
        }
        this.status = newStatus;
    }

    /**
     * @return Observation Value, such as Blood Sugar Level
     */
    public String getObservationValue() {
        return observationValue;
    }

    /**
     * @param newObsValue New Observation Value, such as Blood Sugar Level
     */
    public void setObservationValue(String newObsValue) {
        this.observationValue = newObsValue;
    }

    /**
     * Compares two TreatmentAction objects. Returns false automatically if o is not a TreatmentAction
     * @param o Object to compare
     * @return True if treatment_plan_id, patient_id, action, and dateTime are equal. False otherwise.
     */
    public boolean equal(Object o) {
        //If correct class
        if(TreatmentAction.class == o.getClass()) {
            //Casting to same class
            TreatmentAction other = (TreatmentAction) o;
            //PlanID must match
            if(this.treatment_plan_id.equals(other.treatment_plan_id)) {
                //ActionID must match
                if(this.patient_id.equals(other.patient_id)) {
                    //Date and Time must match
                    if(this.dateTime.equals(other.dateTime)) {
                        //Tasks must match
                        if(this.action.equals(other.action)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @return String formatted for [Month]-[Day]-[Year] Ex: 04/22/2016
     */
    public String getFormattedDate() {

        int month = dateTime.get(GregorianCalendar.MONTH);
        int day = dateTime.get(GregorianCalendar.DAY_OF_MONTH);

        String formattedMonth = (month < 10) ? "0" + String.valueOf(month) : String.valueOf(month);
        String formattedDay = (day < 10) ? "0" + String.valueOf(day) : String.valueOf(day);

        return  formattedMonth+ "/" + formattedDay + "/" + dateTime.get(GregorianCalendar.YEAR);
    }

    /**
     * @param formattedDate String formatted to be "[Month]/[Day]/[Year]"
     * @return integer array of size 3, with index: 0 holding Month, 1 holding Day, 2 holding Year
     */
    public static int[] getDateInfoFromFormattedDate(String formattedDate) {
        String[] data = formattedDate.split("/");
        int[] result = new int[data.length];
        for (int index = 0; index < data.length; index++) {
            result[index] = Integer.valueOf(data[index]);
        }
        return result;
    }

    /**
     * 24 hour clock
     * @return String formatted for [Hour]:[Minute] in 24HR standard
     */
    public String get24HrTime() {
        int hour = dateTime.get(GregorianCalendar.HOUR_OF_DAY);
        int minute = dateTime.get(GregorianCalendar.MINUTE);

        String formattedHour = (hour < 10) ? "0" + String.valueOf(hour) : String.valueOf(hour);
        String formattedMin = (minute < 10) ? "0" + String.valueOf(minute) : String.valueOf(minute);

        return formattedHour + ":" + formattedMin;
    }

    /**
     * 12 hour clock
     * @return String formatted for [Hour]:[Minute] [AM/PM]
     */
    public String get12HrTime() {
        int hour = dateTime.get(GregorianCalendar.HOUR_OF_DAY);
        int minute = dateTime.get(GregorianCalendar.MINUTE);

        String formattedMin = (minute < 10) ? "0" + String.valueOf(minute) : String.valueOf(minute);
        String am_pm = (hour < 12) ? "AM" : "PM";
        String formattedHour= "";

        if(0 == hour) {
            formattedHour = "12";
        } else if(hour > 12) {
            formattedHour = String.valueOf((hour - 12));
        }

        return formattedHour + ":" + formattedMin + " " + am_pm;

    }

}
