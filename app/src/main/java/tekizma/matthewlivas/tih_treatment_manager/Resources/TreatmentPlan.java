package tekizma.matthewlivas.tih_treatment_manager.Resources;

/**
 * This class represents a TreatmentPlan as stored for the Tekizma Integrated Health System (TIH).
 * This class is immutable.
 * Created by MatthewLivas on 4/27/2016.
 */
public class TreatmentPlan {
    /** The plan's unique ID within TIH */
    public final String treatment_plan_id;
    /** The doctor's unique ID within TIH */
    public final String doctor_id;
    /** The patient's unique ID within TIH */
    public final String patient_id;
    /** The medication in this treatment plan */
    public final String medication_name;
    /** The medication dosage */
    public final String dosage;
    /** The frequency with which the medication should be taken */
    public final String frequency;
    /** The start date of this treatment plan */
    public final String start_date;
    /** Then end date of this treatment plan, null represents no specified end date */
    public final String end_date;
    /** The status of the plan Options are: "Pending", "In_Progress", and "Completed"  */
    public final String plan_status;

    /**
     * Basic full constructor
     */
    public TreatmentPlan(String planID, String doctorID, String patientID, String medication,
                         String dosage, String frequency, String startDate, String endDate, String status) {
        this.treatment_plan_id = planID;
        this.doctor_id = doctorID;
        this.patient_id = patientID;
        this.medication_name = medication;
        this.dosage = dosage;
        this.frequency = frequency;
        this.start_date = startDate;
        this.end_date = endDate;
        this.plan_status = status;
    }

}
