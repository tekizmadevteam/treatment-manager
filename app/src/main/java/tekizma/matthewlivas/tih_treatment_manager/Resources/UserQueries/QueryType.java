package tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries;

/**
 * Used to represent the question types we have a template for.
 * Created by MatthewLivas on 5/2/2016.
 */
public enum QueryType {
    DATE, TIME, NUMBER, YES_NO;

    @Override
    public String toString() throws IllegalArgumentException {
        switch (this) {
            case DATE: return "Date";
            case TIME: return "Time";
            case NUMBER: return "Number";
            case YES_NO: return "Yes_No";
            default: throw new IllegalArgumentException("Invalid Enum Option");
        }
    }

    public static QueryType getStatusFromString(String s) {
        switch (s) {
            case "Date": return DATE;
            case "Time": return TIME;
            case "Number": return NUMBER;
            case "Yes_No": return YES_NO;
            default: throw new IllegalArgumentException(s + " is not a valid QueryType option");
        }
    }
}
