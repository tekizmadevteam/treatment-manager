package tekizma.matthewlivas.tih_treatment_manager.UI_Setup;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import java.util.Calendar;

import tekizma.matthewlivas.tih_treatment_manager.R;
import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.*;

public class SetupActivity extends AppCompatActivity {
    private View[] OVERVIEW;
    private QueryResponse[] QUERIES;
    private int CURRENT_PAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        QUERIES = getQuery();
        int numberOfPages = QUERIES.length;

        OVERVIEW = new View[numberOfPages];

        CarouselView carouselView = (CarouselView) findViewById(R.id.carousel);
        carouselView.setPageCount(numberOfPages);

        carouselView.setViewListener(new ViewListener() {
            @Override
            public View setViewForPosition(int position) {
                /*  Creates initial carousel
                 *
                 *  questionBox is visualizing text queries.
                 *  buttonSet is the button for user to click to initiate response input
                 */
                OVERVIEW[position] = getLayoutInflater().inflate(R.layout.setup_inflatable, null);
                TextView questionBox = (TextView) OVERVIEW[position].findViewById(R.id.temp_view);
                QueryResponse current = QUERIES[position];
                Button buttonSet = (Button) OVERVIEW[position].findViewById(R.id.button_setup_set);

                questionBox.setText(current.question);

                switch (current.query_type) {
                    case DATE:
                        buttonSet.setText("Set date");
                        break;
                    case NUMBER:
                        buttonSet.setText("Set value");
                        break;
                    case YES_NO:
                        buttonSet.setText("Set option");
                        break;
                    case TIME:
                        buttonSet.setText("Set time");
                        break;
                }

                return OVERVIEW[position];
            }
        });

        carouselView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                CURRENT_PAGE = position;
            }

            @Override
            public void onPageSelected(int position) {
                CURRENT_PAGE = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    private QueryResponse[] getQuery() {
        //Returns array of QuereyResponse questions to be answered by patient.
        //TODO: Read information from file system, currently returns dummy array for testing purposes.

        QueryResponse[] array = {
                new DateResponse("1","What date?"),
                new NumberResponse("2","How many?"),
                new TimeResponse("3","What time?"),
                new YesNoResponse("4","Take medication?")};

        return array;
    }

    public void setup_click(View v) {
        Button setButton = (Button) OVERVIEW[CURRENT_PAGE].findViewById(R.id.button_setup_set);

        if (v.getId() == R.id.button_setup_yes) {
            setButton.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.button_setup_no) {
            setButton.setVisibility(View.GONE);
        } else if (v.getId() == R.id.button_setup_set) {
            createDialogBox(QUERIES[CURRENT_PAGE]);
        }
    }

    private void createDialogBox(QueryResponse query) {
        /*  Creates and displays dialog box.
         *
         *  Calendar is used for setting TimePicker and DatePicker to spawn with current time/date
         */
        QueryType queryType = query.query_type;

        Calendar cal = Calendar.getInstance();
        int todayHour = cal.getTime().getHours(),
                todayMin = cal.getTime().getMinutes(),
                todayDate = cal.getTime().getDate(),
                todayMonth = cal.getTime().getMonth(),
                todayYear = cal.getTime().getYear() + 1900;

        if (queryType == QueryType.YES_NO) {
            new AlertDialog.Builder(this)
                    .setTitle("Yes or No?")
                    .setMessage(query.question)
                    .setPositiveButton(R.string.response_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Button setButton = (Button) OVERVIEW[CURRENT_PAGE].findViewById(R.id.button_setup_set);

                            setButton.setText("YES");
                            QUERIES[CURRENT_PAGE].setUserResponse(true);
                        }
                    })
                    .setNegativeButton(R.string.response_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Button setButton = (Button) OVERVIEW[CURRENT_PAGE].findViewById(R.id.button_setup_set);

                            setButton.setText("NO");
                            QUERIES[CURRENT_PAGE].setUserResponse(false);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else if (queryType == QueryType.TIME) {
            new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Button setButton = (Button) OVERVIEW[CURRENT_PAGE].findViewById(R.id.button_setup_set);
                    String response = "";

                    if (hourOfDay == 0)
                        response = "24";
                    else if (hourOfDay < 10)
                        response += "0" + hourOfDay;
                    else
                        response += hourOfDay;

                    response += ":";

                    if (minute < 10)
                        response += "0";
                    response += minute;

                    setButton.setText(response);
                    QUERIES[CURRENT_PAGE].setUserResponse(response);
                }
            }, todayHour, todayMin, false).show();
        } else if (queryType == QueryType.DATE) {
            new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Button setButton = (Button) OVERVIEW[CURRENT_PAGE].findViewById(R.id.button_setup_set);
                    String response = "";

                    monthOfYear ++;

                    if (monthOfYear < 10)
                        response += "0";
                    response += monthOfYear + "/";

                    if (dayOfMonth < 10)
                        response += "0";
                    response += dayOfMonth + "/" + year;

                    setButton.setText(response);

                    QUERIES[CURRENT_PAGE].setUserResponse(response);
                }
            }, todayYear, todayMonth, todayDate).show();
        } else if (queryType == QueryType.NUMBER) {
            NumberPicker picker = new NumberPicker(this);
            new AlertDialog.Builder(this)
                    .setTitle("Number input")
                    .setMessage(query.question)
                    .setView(picker)
                    .setPositiveButton("set", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Button setButton = (Button) OVERVIEW[CURRENT_PAGE].findViewById(R.id.button_setup_set);
                        }
                    }).show();

            QUERIES[CURRENT_PAGE].setUserResponse(picker.getValue());
        }
    }
}
