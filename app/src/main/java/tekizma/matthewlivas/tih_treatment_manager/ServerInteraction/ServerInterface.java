package tekizma.matthewlivas.tih_treatment_manager.ServerInteraction;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * This class' methods provide a simplified way to interface with the Tekizma TIH Server dedicated
 * to supporting this application.
 *
 * Not currently supporting paging.
 *
 * THIS CLASS IS NOT THREAD SAFE.
 *
 * Initially created by MatthewLivas on 4/13/2016.
 */
public class ServerInterface {

    /**
     * Acts as the base query URL. The access endpoint for the server. Full queries will look like:
     * <BASE_URL><Server_Resource>?<ResourceID or query string with & seperated parameters>
     */
    private static final String BASE_URL = "https://tekizma.com/treatmentManager/";

    /**
     * This method queries the server for information and saves the response into the specified file.
     * Will overwrite any existing file with the same name. If the server returns a non 200 code, then
     * this method will save the response to the ERROR_FILE_NAME and return the name of the file it
     * has written to
     * @param resourceType Type of resource being requested
     * @param parameters fully URL escaped parameters representing the data being requested
     * @param outputLocation directory in phone memory to store the file
     * @param baseFileName requested file name without the file extension
     * @return file name of where the response was written
     */
    private static String queryServer(Server_Resources resourceType, String parameters, File outputLocation,
                                      String baseFileName) {
        /*
        File to be written.
         */
        File outputFile;
        /*
        File name of the file to be written.
         */
        String fullFileName = baseFileName + MemoryManagement.FILE_EXTENSION;
        /*
        Used to open an input stream from the server.
         */
        URL URLconnection;
        /*
        Used to hold the URL connection and handle URL specifications.
         */
        HttpsURLConnection currentConnection = null;
        /*
        Used to read the input stream.
         */
        BufferedReader inputBuffer;
        /*
        Used as the in-between from the inputBuffer and the fileWriter.
         */
        String inputLine;
        /*
        Actually write the file
         */
        PrintWriter fileWriter = null;

        /* If outputLocation isn't a directory, throw an invalid argument exception */
        if(!outputLocation.isDirectory()) {
            throw new IllegalArgumentException("outputLocation must be a directory location.");
        }

        /* Create a link to the file to be written */
        outputFile = new File(outputLocation, fullFileName);

        /* If the file doesn't exist, create it. */
        if(!outputFile.exists()) {
            try{
                outputFile.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException("Error creating " + outputFile.getName());
            }
        }

        /* Query the server and write the response to outputFile */
        try {

            /* Build the query URL */
            URLconnection = new URL(BASE_URL + resourceType.toString() + "?" + parameters);

            /* Make the URL into a HttpsURLConnection so the accept type can be added
             * Will also be needed to add authentication to the server query */
            currentConnection = (HttpsURLConnection) URLconnection.openConnection();

            /* Ask the server to respond in JSON */
            currentConnection.setRequestProperty("Accept", "application/json");

            /* Get the server response's status code */
            int statusCode = currentConnection.getResponseCode();

            /* If the status code is not 200, count it as an error */
            if(200 != statusCode) {
                outputFile = new File(outputLocation, MemoryManagement.ERROR_FILE_NAME + MemoryManagement.FILE_EXTENSION);

                /* If the file doesn't exist, create it. */
                if(!outputFile.exists()) {
                    try{
                        outputFile.createNewFile();
                    } catch (IOException e) {
                        throw new RuntimeException("Error creating " + outputFile.getName());
                    }
                }


            }

            /* Create the print writer that will write the server response */
            fileWriter = new PrintWriter(outputFile);

            /* Make a buffered reader for the server's response */
            inputBuffer = new BufferedReader(new InputStreamReader(currentConnection.getInputStream()));

            /* Write the server's response to outputFile */
            while((inputLine = inputBuffer.readLine()) != null) {
                fileWriter.write(inputLine + "\n");
            }

            /* Close the connection to the server */
            currentConnection.disconnect();

            /* Close the input buffer */
            inputBuffer.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error finding " + outputFile.getName());
        } catch (MalformedURLException e) {
            throw new RuntimeException("Malformed URL. " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("IOException. " + e.getMessage());
        } finally {
            /* Close the PrintWriter */
            if(null != fileWriter) {
                fileWriter.close();
            }

            /* Close the connection to the server */
            if(null != currentConnection) {
                currentConnection.disconnect();
            }
        }

        /* Return the written file name */
        return outputFile.getName();
    }



}
