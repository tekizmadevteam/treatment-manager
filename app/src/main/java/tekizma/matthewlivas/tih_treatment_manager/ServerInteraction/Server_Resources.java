package tekizma.matthewlivas.tih_treatment_manager.ServerInteraction;

/**
 * This class is used to represent the different Resources available on the RESTful Server.
 * Created by MatthewLivas on 5/4/2016.
 */
public enum Server_Resources {

    PATIENT, DOCTOR, ACTION_LIST_ITEM, TREATMENT_PLAN;

    @Override
    public String toString() {
        switch (this) {
            case PATIENT: return "patient";
            case DOCTOR: return "doctor";
            case ACTION_LIST_ITEM: return "actionListItem";
            case TREATMENT_PLAN: return "treatmentPlan";
            default: throw new RuntimeException("Invalid enum state.");
        }
    }
}
