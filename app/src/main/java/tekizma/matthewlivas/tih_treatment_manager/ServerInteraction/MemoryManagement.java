package tekizma.matthewlivas.tih_treatment_manager.ServerInteraction;

import android.content.Context;

import tekizma.matthewlivas.tih_treatment_manager.Resources.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

/**
 * This class has all static methods to be used to interact with Android memory to support the
 * current memory model.
 *
 * Currently, all information will be stored in arbitrary files in JSON format.
 *
 * To retrieve information in files, the file will be parsed into a JSON object. This JSON object
 * will subsequently be parsed into the object(s) it represents, and those objects will then be used
 * in the rest of the program.
 *
 * To store information in files, the object(s) to be stored will be transformed into JSON format
 * and then written out to a file.
 *
 * Created by MatthewLivas on 5/13/2016.
 */
public class MemoryManagement {

    /**
     * File extension to save files as locally
     */
    public static final String FILE_EXTENSION = ".txt";

    /**
     * File name for storing treatment plans.
     */
    public static final String TREATMENT_PLAN_FILE_NAME = "TreatmentPlanFile";

    /**
     * File name for storing patient information.
     */
    public static final String PATIENT_FILE_NAME = "PatientFile";

    /**
     * File name for storing doctor information.
     */
    public static final String DOCTOR_FILE_NAME = "DoctorFile";

    /**
     * File name for storing the treatment action list.
     */
    public static final String ACTION_LIST_FILE_NAME = "ActionListFile";

    /**
     * File name for storing the most recently encountered non HTTP200 status code's message.
     */
    public static final String ERROR_FILE_NAME = "ErrorFile";

    /**
     * File name for storing the most recent set of questions requested to be ansered
     * by the server.
     */
    public static final String PLAN_SETUP_QUESTIONS_FILE_NAME = "PlanSetupQuestionsFile";

    /**
     * This method loads some simple test messages from the server.
     * Should be removed from the production run
     * @return true if success, false otherwise
     */
    public static boolean loadSimpleTestServerMessages(Context context) {
        //TODO remove this method for production release
        final String treatmentActionMessage = "{\"treatment_actions\":[{\"Status\":\"Pending\",\"Action\":\"Take Tylenol 10ml\",\"patient_id\":\"johndoe1\",\"observation\":\"\",\"Time\":\"08:00\",\"treatment_plan_id\":\"johndoe_plan_1\",\"Date\":\"04/22/2016\"},{\"Status\":\"Pending\",\"Action\":\"Take Tylenol 10ml\",\"patient_id\":\"johndoe1\",\"observation\":\"\",\"Time\":\"21:00\",\"treatment_plan_id\":\"johndoe_plan_1\",\"Date\":\"04/22/2016\"}]}";
        final String doctorMessage = "{\"doctors\":[{\"doctor_id\":\"drwho1\",\"dr_email\":\"xyz@abc.com\",\"dr_Fname\":\"Strange\",\"provider_id\":\"acmeclinic\",\"dr_Lname\":\"Who\"}]}";
        final String patientMessage = "{\"patients\":[{\"doctor_id\":\"drwho1\",\"password\":\"<<Hashed password for 'Strange'>>\",\"patient_id\":\"johndoe1\",\"date_of_birth\":\"12/12/1980\",\"patient_email\":\"xyz@acmeclinic.com\",\"last_name\":\"Doe\",\"phone_number\":\"999-999-9999\",\"first_name\":\"John\"}]}";
        final String treatmentPlanMessage = "{\"treatment_plans\":[{\"end_date\":\"04/22/2016\",\"doctor_id\":\"drwho1\",\"dosage\":\"10 ml\",\"patient_id\":\"johndoe1\",\"medication_name\":\"Tylenol\",\"treatment_plan_id\":\"johndoe_plan_1\",\"frequency\":\"Twice daily\",\"start_date\":\"04/22/2016\",\"plan_status\":\"Pending\"}]}";
        final String setupQuestionMessage = "{\"doctor_id\":\"drwho1\",\"patient_id\":\"johndoe1\",\"treatment_questions\":[{\"query\":\"Do you have your prescription filled?\",\"response_type\":\"Yes_No\",\"question_id\":\"prescription1\"},{\"query\":\"When would you like to begin your Treatment Plan?\",\"response_type\":\"Date\",\"question_id\":\"beginDate1\"},{\"query\":\"What time would you prefer to take your first dose of Tylenol?\",\"response_type\":\"Time\",\"question_id\":\"medicine1_time1\"},{\"query\":\"How many cats live in your household?\",\"response_type\":\"Number\",\"question_id\":\"num_cats\"}],\"treatment_plan_id\":\"johndoe_plan_1\"}";
        final String testErrorMessage = "{\"error\": \"Error not found.\",\"code\": \"404\"}";

        JSONObject message = null;
        boolean returnVal = true;

        try {
            message = new JSONObject(treatmentActionMessage);
        } catch (JSONException e) {
            returnVal = false;
            e.printStackTrace();
        }
        if(null != message) {
            writeJSONObjectToFile(message, getActionListFile(context));
        }

        try {
            message = new JSONObject(doctorMessage);
        } catch (JSONException e) {
            returnVal = false;
            e.printStackTrace();
        }
        if(null != message) {
            writeJSONObjectToFile(message, getDoctorFile(context));
        }

        try {
            message = new JSONObject(patientMessage);
        } catch (JSONException e) {
            returnVal = false;
            e.printStackTrace();
        }
        if(null != message) {
            writeJSONObjectToFile(message, getPatientFile(context));
        }

        try {
            message = new JSONObject(treatmentPlanMessage);
        } catch (JSONException e) {
            returnVal = false;
            e.printStackTrace();
        }
        if(null != message) {
            writeJSONObjectToFile(message, getTreatmentPlanFile(context));
        }

        try {
            message = new JSONObject(setupQuestionMessage);
        } catch (JSONException e) {
            returnVal = false;
            e.printStackTrace();
        }
        if(null != message) {
            writeJSONObjectToFile(message, getSetupQuestionsFile(context));
        }

        try {
            message = new JSONObject(testErrorMessage);
        } catch (JSONException e) {
            returnVal = false;
            e.printStackTrace();
        }
        if(null != message) {
            writeJSONObjectToFile(message, getErrorFile(context));
        }

        return returnVal;

    }

    /**
     * @return the internal storage directory for this application.
     */
    public static File getInternalStorageDirectory(Context context) {
            return context.getFilesDir();
    }

    /**
     * Use this to get a file with the default file extension at the root level of internal storage
     * @param fileName name of file with default file extension
     * @return file representing the location of the specified file at the root level of
     * internal storage
     */
    private static File getInternalFileLocation(String fileName, Context context) {
        return new File(getInternalStorageDirectory(context), fileName + FILE_EXTENSION);
    }

    /**
     * Use this to get a reference to the TreatmentPlan master copy
     * @return File representing the saved TreatmentPlan master copy
     */
    public static File getTreatmentPlanFile(Context context) {
        return getInternalFileLocation(TREATMENT_PLAN_FILE_NAME, context);
    }

    /**
     * Use this to get a reference to the Patient master copy
     * @return File representing the saved Patient master copy     */
    public static File getPatientFile(Context context) {
        return getInternalFileLocation(PATIENT_FILE_NAME, context);
    }

    /**
     * Use this to get a reference to the Doctor master copy
     * @return File representing the saved Doctor master copy
     */
    public static File getDoctorFile(Context context) {
        return getInternalFileLocation(DOCTOR_FILE_NAME, context);
    }

    /**
     * Use this to get a reference to the ActionList master copy
     * @return File representing the saved ActionList master copy
     */
    public static File getActionListFile(Context context) {
        return getInternalFileLocation(ACTION_LIST_FILE_NAME, context);
    }

    /**
     * Use this to get a reference to the most recent Error encountered
     * @return File representing the saved most recent Error encounterd
     */
    public static File getErrorFile(Context context) {
        return getInternalFileLocation(ERROR_FILE_NAME, context);
    }

    /**
     * Use this to get a reference to the SetupQuestions master copy
     * @return File representing the saved SetupQuestions master copy
     */
    public static File getSetupQuestionsFile(Context context) {
        return getInternalFileLocation(PLAN_SETUP_QUESTIONS_FILE_NAME, context);
    }

    /**
     * @param file to parse into a JSON Object
     * @return JSON Object
     */
    public static JSONObject readFromFileJSON(File file) {
        String content = null; //To hold the scanned file
        Scanner scan = null; //To read the file
        Scanner scanner = null; //To get scan with EOF delimiter
        //Scan the whole file at once
        try {
            scanner = new Scanner(file);
            scan = scanner.useDelimiter("\\Z");
            content = scan.next();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(file.getAbsolutePath() + " was not found.");
        } finally {
            if(null != scanner) {
                scanner.close();
            }
            if(null != scan) {
                scan.close();
            }
        }

        //Put the scanned file into a JSON object
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(content);
        } catch (JSONException e) {
            throw new RuntimeException("The file was not a valid JSON file");
        }

        return jsonObj;
    }

    /**
     * @param toWrite JSON Object to save
     * @param outLocation File location to save it to
     */
    public static void writeJSONObjectToFile(JSONObject toWrite, File outLocation) {

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(outLocation);
            writer.write(toWrite.toString());
        } catch (IOException e) {
            throw new RuntimeException("Error encountered in writing JSON file out.");
        } finally {
            if(null != writer) {
                writer.close();
            }
        }
    }

    /**
     * Retrieve the master list of Treatment Plans that are saved locally on this device.
     * Note any changes made to these objects will not be stored until saved using another
     * method in this class.
     * @return list of saved Treatment Plans
     */
    public static List<TreatmentPlan> getSavedTreatmentPlans(Context context) {
        JSONObject message = readFromFileJSON(getTreatmentPlanFile(context));

        return JSONTranslator.parseTreatmentPlanMessage(message);
    }

    /**
     * Retrieve the master list of Treatment Actions that are saved locally on this device.
     * Note any changes made to these objects will not be stored until saved using another
     * method in this class.
     * @return list of stored TreatmentActions
     */
    public static List<TreatmentAction> getSavedActionsList(Context context) {
        JSONObject message = readFromFileJSON(getActionListFile(context));

        return JSONTranslator.parseTreatmentActionMessage(message);
    }

    /**
     * Retrieve the master list of Doctors that are saved locally on this device.
     * Note any changes made to these objects will not be stored until saved using
     * another method in this class.
     * @return list of stored Doctors
     */
    public static List<Doctor> getSavedDoctors(Context context) {
        JSONObject message = readFromFileJSON(getDoctorFile(context));

        return JSONTranslator.parseDoctorMessage(message);
    }

    /**
     * Retrieve the master list of Patients that are saved locally on this device.
     * Note that any changes made to these objects will not be stored until saved using
     * another method in this class.
     * @return List of patients representing the master copy
     */
    public static List<Patient> getSavedPatients(Context context) {
        JSONObject message = readFromFileJSON(getPatientFile(context));

        return JSONTranslator.parsePatientMessage(message);
    }

    /**
     * Retrieve the master copy of the most recent set of questions requested
     * by the server
     * @return TreatmentSetupQuestions representing the master copy
     */
    public static TreatmentSetupQuestions getSavedSetupQuestions(Context context) {
        JSONObject message = readFromFileJSON(getSetupQuestionsFile(context));

        return JSONTranslator.parseTreatmentSetupQuestionsMessage(message);
    }

    /**
     * Retrieve the most recent error encountered that is saved locally on this device.
     * Note that any change made to the returned object will not be stored until saved
     * using another method in this class.
     * @return JSON representation of the most recently encoutnered error
     */
    public static JSONObject getSavedError(Context context) {
        return readFromFileJSON(getErrorFile(context));
    }

}
