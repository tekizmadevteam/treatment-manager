package tekizma.matthewlivas.tih_treatment_manager.ServerInteraction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import tekizma.matthewlivas.tih_treatment_manager.Resources.ActionStatus;
import tekizma.matthewlivas.tih_treatment_manager.Resources.Doctor;
import tekizma.matthewlivas.tih_treatment_manager.Resources.Patient;
import tekizma.matthewlivas.tih_treatment_manager.Resources.TreatmentAction;
import tekizma.matthewlivas.tih_treatment_manager.Resources.TreatmentPlan;
import tekizma.matthewlivas.tih_treatment_manager.Resources.TreatmentSetupQuestions;
import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.DateResponse;
import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.NumberResponse;
import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.QueryResponse;
import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.QueryType;
import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.TimeResponse;
import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.YesNoResponse;

/**
 * This class will provide methods for creating and decoding JSON objects into/out of any relevant
 * objects for local use.
 * Created by MatthewLivas on 4/20/2016.
 */
public class JSONTranslator {

    /**
     * Pass in a Doctor object, return a JSONObject representing that doctor.
     */
    public static JSONObject createJSONFromDoctor(Doctor doc) {
        JSONObject output = new JSONObject();

        try {
            output.put("doctor_id", doc.doctor_id);
            output.put("provider_id", doc.provider_id);
            output.put("dr_Fname", doc.dr_first_name);
            output.put("dr_Lname", doc.dr_last_name);
            output.put("dr_email", doc.dr_email);
        } catch (JSONException e) {
            throw new RuntimeException("A problem creating the Doctor JSON Object has occurred");
        }
        return output;
    }

    /**
     * Pass in a JSONObject representing a Doctor, return an equivalent Doctor object.
     */
    public static Doctor createDoctorFromJSON(JSONObject obj) {

        String doctorID, providerID, firstName, lastName, email;

        try {
            doctorID = obj.get("doctor_id").toString();
            providerID = obj.get("provider_id").toString();
            firstName = obj.get("dr_Fname").toString();
            lastName = obj.get("dr_Lname").toString();
            email = obj.get("dr_email").toString();
        } catch (JSONException e) {
            throw new IllegalArgumentException("Parameter does not fully represent a Doctor");
        }

        return new Doctor(doctorID, providerID, firstName, lastName, email);

    }

    /**
     * Pass in a Patient object, returning a JSONObject representing that patient.
     */
    public static JSONObject createJSONFromPatient(Patient patient) {
        JSONObject output = new JSONObject();

        try {
            output.put("patient_id", patient.patient_id);
            output.put("doctor_id", patient.doctor_id);
            output.put("patient_email", patient.patient_email);
            output.put("password", patient.getPassword());
            output.put("first_name", patient.first_name);
            output.put("last_name", patient.last_name);
            output.put("phone_number", patient.phone_number);
            output.put("date_of_birth", patient.date_of_birth);
        } catch (JSONException e) {
            throw new RuntimeException("A problem creating the Patient JSON Object has occurred");
        }
        return output;
    }

    /**
     * Pass in a JSONObject representing a Patient, returning an equivalent Patient object
     */
    public static Patient createPatientFromJSON(JSONObject obj) {
        String patientID, doctorID, email, password, firstName, lastName, phoneNum, dob;

        try {
            patientID = obj.get("patient_id").toString();
            doctorID = obj.get("doctor_id").toString();
            email = obj.get("patient_email").toString();
            password = obj.get("password").toString();
            firstName = obj.get("first_name").toString();
            lastName = obj.get("last_name").toString();
            phoneNum = obj.get("phone_number").toString();
            dob = obj.get("date_of_birth").toString();

        } catch (JSONException e) {
            throw new IllegalArgumentException("Parameter does not fully represent a Patient");
        }
        return new Patient(patientID, doctorID, email, password, firstName, lastName, phoneNum, dob);
    }

    /**
     * Pass in a TreatmentAction object, return a JSONObject representing that TreatmentAction.
     */
    public static JSONObject createJSONFromTreatmentAction(TreatmentAction action) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("patient_id", action.getPatientID());
            obj.put("treatment_plan_id", action.getTreatmentPlanID());
            obj.put("Action", action.getAction());
            obj.put("Date", action.getFormattedDate());
            obj.put("Time", action.get24HrTime());
            obj.put("Status", action.getStatus().toString());

            String obsVal = action.getObservationValue();

            if (null == obsVal) {
                obsVal = "";
            }

            obj.put("observation", obsVal);

        } catch (JSONException e) {
            throw new RuntimeException("A problem creating the TreatmentAction JSON Object has occurred");
        }
        return obj;
    }

    /**
     * Pass in a JSONObject representing a TreatmentAction, returning an equivalent TreatmentAction.
     */
    public static TreatmentAction createTreatmentActionFromJSON(JSONObject obj) {
        String patientID, treatmentPlanID, action, obsValue = "";
        GregorianCalendar calendar;
        ActionStatus status;
        int[] dateInfo;
        String[] strTimeInfo;
        int[] timeInfo;

        try {
            patientID = obj.get("patient_id").toString();
            treatmentPlanID = obj.get("treatment_plan_id").toString();
            action = obj.get("Action").toString();
            status = ActionStatus.getStatusFromString(obj.get("Status").toString());
            if(obj.has("observation:")) {
                obsValue = obj.get("observation").toString();
            }
            dateInfo = TreatmentAction.getDateInfoFromFormattedDate(obj.get("Date").toString());
            strTimeInfo = obj.get("Time").toString().split(":");

            timeInfo = new int[strTimeInfo.length];
            for(int i = 0; i < strTimeInfo.length; i++) {
                timeInfo[i] = Integer.valueOf(strTimeInfo[i]);
            }

            calendar = new GregorianCalendar(dateInfo[2], dateInfo[0], dateInfo[1], timeInfo[0], timeInfo[1]);
        } catch (JSONException e) {
            throw new IllegalArgumentException("Parameter does not fully represent a TreatmentAction: " + e.getLocalizedMessage());
        }

        return new TreatmentAction(treatmentPlanID, patientID, action, calendar, status, obsValue);
    }

    /**
     * Pass in a TreatmentPlan object, returning a JSONObject representing that TreatmentPlan.
     */
    public static JSONObject createJSONFromTreatmentPlan(TreatmentPlan plan) {
        JSONObject output = new JSONObject();

        try {
            output.put("treatment_plan_id", plan.treatment_plan_id);
            output.put("doctor_id", plan.doctor_id);
            output.put("patient_id", plan.patient_id);
            output.put("medication_name", plan.medication_name);
            output.put("dosage", plan.dosage);
            output.put("frequency", plan.frequency);
            output.put("start_date", plan.start_date);
            output.put("end_date", plan.end_date);
            output.put("plan_status", plan.plan_status);
        } catch (JSONException e) {
            throw new RuntimeException("A problem creating the TreatmentPlan JSON Object has occurred");
        }
        return output;
    }

    /**
     * Pass in a JSONObject representing a TreatmentPlan, returning an equivalent TreatmentPlan
     */
    public static TreatmentPlan createTreatmentPlanFromJSON(JSONObject obj) {
        String treatmentID, doctorID, patientID, medication, dosage, frequency, start, end, status;

        try {
            treatmentID = obj.getString("treatment_plan_id");
            doctorID = obj.getString("doctor_id");
            patientID = obj.getString("patient_id");
            medication = obj.getString("medication_name");
            dosage = obj.getString("dosage");
            frequency = obj.getString("frequency");
            start = obj.getString("start_date");
            end = obj.getString("end_date");
            status = obj.getString("plan_status");
        } catch (JSONException e) {
            throw new IllegalArgumentException("Parameter does not fully represent a TreatmentPlan");
        }
        return new TreatmentPlan(treatmentID, doctorID, patientID, medication, dosage, frequency, start, end, status);
    }

    /**
     * Pass in a single QueryResponse object, returning a JSONObject representing that QueryResponse.
     */
    public static JSONObject createJSONFromQueryResponse(QueryResponse response) {
        JSONObject output = new JSONObject();

        try {

            output.put("question_id", response.question_id);
            output.put("query", response.question);
            output.put("response_type", response.query_type.toString());

            if(!response.getUserResponse().equals("")) {
                output.put("user_response", response.getUserResponse());
            }

        } catch (JSONException e) {
            throw new RuntimeException("A problem creating the TreatmentPlan JSON Object has occurred");
        }
        return output;
    }

    /**
     * Pass in a JSONObject representing a QueryResponse object, return a QueryResponse equivalent to
     * that JSONObject. Will be the correct concrete implementation for the QueryType.
     */
    public static QueryResponse createQueryResponseFromJSON(JSONObject obj) {
        String questionID, question, responseType, userResponse;

        try {
            questionID = obj.getString("question_id");
            question = obj.getString("query");
            responseType = obj.getString("response_type");
            if(obj.has("user_response")) {
                userResponse = obj.getString("user_response");
            } else {
                userResponse = "";
            }
        } catch (JSONException e) {
            throw new IllegalArgumentException("Parameter does not fully represent a QueryResponse");
        }

        QueryType type = QueryType.getStatusFromString(responseType);

        QueryResponse response = null;

        switch (type) {
            case DATE: response = new DateResponse(questionID, question);
                break;
            case TIME: response = new TimeResponse(questionID, question);
                break;
            case NUMBER: response = new NumberResponse(questionID, question);
                break;
            case YES_NO: response = new YesNoResponse(questionID, question);
                break;
//            default: new IllegalArgumentException("Invalid QueryType.");
        }

        //TODO potential source of a leak/security breach. Needs to preserve invariants of
        //individual QueryResponse types
        if(!userResponse.equals("")) {
            response.setUserResponse(userResponse);
        }

        return response;
    }

    /**
     * Use to "PrettyPrint" json with 4 indent spaces
     * @param o JSON object to print
     * @return string formatted with the pretty printed JSON object
     * @throws JSONException standard JSON exception
     */
    public static String prettyPrintJSON(JSONObject o) {
        String output = "testMessage";

        try {
            output = o.toString(4);
        } catch (JSONException e) {
            output = "An error was encountered PrettyPrinting this JSON object";
        }
        finally {
            return output;
        }
    }

    /**
     * Pass in a JSONObject representing a full JSON message from the server containing Doctor
     * information. Returns a list of the contained Doctor objects.
     */
    public static List<Doctor> parseDoctorMessage(JSONObject obj) {
        List<Doctor> outputList = new LinkedList<Doctor>();

        try {

            if (obj.has("doctors")) {
                JSONArray doctorArray = obj.getJSONArray("doctors");

                for(int index = 0; index < doctorArray.length(); index++) {
                    outputList.add(createDoctorFromJSON(doctorArray.getJSONObject(index)));
                }


            } else {
                throw new IllegalArgumentException("Parameter does not represent a Doctor Message.");
            }


        } catch (JSONException e) {
            throw new RuntimeException("An error has occurred while parsing the Doctor Message.");
        }
        return outputList;
    }

    /**
     * Pass in a JSONObject representing a full JSON message from the server containing Patient
     * information. Returns a list of the contained Patient objects.
     */
    public static List<Patient> parsePatientMessage(JSONObject obj) {
        List<Patient> outputList = new LinkedList<Patient>();

        try {

            if (obj.has("patients")) {
                JSONArray doctorArray = obj.getJSONArray("patients");

                for(int index = 0; index < doctorArray.length(); index++) {
                    outputList.add(createPatientFromJSON(doctorArray.getJSONObject(index)));
                }


            } else {
                throw new IllegalArgumentException("Parameter does not represent a Patient Message.");
            }


        } catch (JSONException e) {
            throw new RuntimeException("An error has occurred while parsing the Patient Message.");
        }
        return outputList;
    }

    /**
     * Pass in a JSONObject representing a full JSON message from the server containing
     * TreatmentActions. Returns a list of the contained TreatmentAction objects.
     */
    public static List<TreatmentAction> parseTreatmentActionMessage(JSONObject obj) {
        List<TreatmentAction> outputList = new LinkedList<TreatmentAction>();

        try {

            if (obj.has("treatment_actions")) {
                JSONArray doctorArray = obj.getJSONArray("treatment_actions");

                for(int index = 0; index < doctorArray.length(); index++) {
                    outputList.add(createTreatmentActionFromJSON(doctorArray.getJSONObject(index)));
                }


            } else {
                throw new IllegalArgumentException("Parameter does not represent a TreatmentAction Message.");
            }


        } catch (JSONException e) {
            throw new RuntimeException("An error has occurred while parsing the TreatmentAction Message.");
        }
        return outputList;
    }

    /**
     * Pass in a JSONObject representing a full JSON message from the server containing
     * TreatmentActions. Returns a list of the contained TreatmentAction objects.
     */
    public static List<TreatmentPlan> parseTreatmentPlanMessage(JSONObject obj) {
        List<TreatmentPlan> outputList = new LinkedList<TreatmentPlan>();

        try {

            if (obj.has("treatment_plans")) {
                JSONArray doctorArray = obj.getJSONArray("treatment_plans");

                for(int index = 0; index < doctorArray.length(); index++) {
                    outputList.add(createTreatmentPlanFromJSON(doctorArray.getJSONObject(index)));
                }


            } else {
                throw new IllegalArgumentException("Parameter does not fully represent a TreatmentPlan Message.");
            }


        } catch (JSONException e) {
            throw new RuntimeException("An error has occurred while parsing the TreatmentPlan Message.");
        }
        return outputList;
    }

    /**
     * Pass in a JSONObjet representing a full JSON message from the server containing Treatment
     * Plan Setup questions. Returns a TreatmentSetupQuestions object populated with the contained
     * QueryResponses
     */
    public static TreatmentSetupQuestions parseTreatmentSetupQuestionsMessage(JSONObject obj) {
        String planID, doctorID, patientID;

        TreatmentSetupQuestions questionBundle = null;

        try {
            planID = obj.getString("treatment_plan_id");
            doctorID = obj.getString("doctor_id");
            patientID = obj.getString("patient_id");

            questionBundle = new TreatmentSetupQuestions(planID, doctorID, patientID);

            if(!obj.has("treatment_questions")) {
                throw new IllegalArgumentException("Parameter does not fully represent a TreatmentSetupQuestions Message");
            }

            JSONArray questionArray = obj.getJSONArray("treatment_questions");

            for(int index = 0; index < questionArray.length(); index++) {
                questionBundle.addQueryResponse(createQueryResponseFromJSON(questionArray.getJSONObject(index)));
            }

        } catch (JSONException e) {
            throw new RuntimeException("An error has occurred while parsing the TreatmentSetupQuestions Message. " + e.getLocalizedMessage());
        }
        return questionBundle;
    }

    public static JSONObject createJSONFromTreatmentSetupQuestion(TreatmentSetupQuestions t) {
        JSONObject obj = new JSONObject();

        try {
            obj.put("treatment_plan_id", t.plan_id);
            obj.put("doctor_id", t.doctor_id);
            obj.put("patient_id", t.patient_id);

            JSONArray questionArray = new JSONArray();

            QueryResponse[] array = t.getQuestionArray();

            for(QueryResponse q : array) {
                questionArray.put(JSONTranslator.createJSONFromQueryResponse(q));
            }

            obj.put("treatment_questions", questionArray);

        } catch (JSONException e) {
            throw new RuntimeException("An error has occurred while creating the JSON Object: " + e.getLocalizedMessage());
        }

        return obj;
    }

}
