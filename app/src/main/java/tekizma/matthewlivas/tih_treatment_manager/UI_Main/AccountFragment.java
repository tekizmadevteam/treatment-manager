package tekizma.matthewlivas.tih_treatment_manager.UI_Main;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tekizma.matthewlivas.tih_treatment_manager.R;
import tekizma.matthewlivas.tih_treatment_manager.UI_Data.DoctorView;

/**
 * Created by Nate on 4/8/2016.
 */
public class AccountFragment extends android.support.v4.app.Fragment {
    private View OVERVIEW = null;   //Uninflated view

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        OVERVIEW = inflater.inflate(R.layout.nav_choice_account, container, false);
        EditText input = (EditText) OVERVIEW.findViewById(R.id.add_new_code);
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                /*  Changing parameter in this method, like erasing its contents
                *   will recursively call this method, so the if statement is there to prevent indfinte loop.
                */
                if (s.length() != 0) {
                    String inputText = s.toString();
                    if (inputText.contains("\n")) {
                        s.delete(0, s.length());
                        //TODO: send inputText to server to check and add new treatment plan code
                    }
                }
            }
        });

        createClinitianList();

        return OVERVIEW;
    }

    private void createClinitianList() {
        //Creates Views for healthcare professionals and places them in linearLayout, which is obtained using OVERVIEW
        LinearLayout linearLayout = (LinearLayout) OVERVIEW.findViewById(R.id.doctor_list);
        RelativeLayout.LayoutParams params;
        RelativeLayout doctorInfo;

        List<DoctorView> doctorList = getDoctorList();
        TextView name, codeID, details;

        for (int counter = 0; counter < doctorList.size(); counter ++) {
            DoctorView current = doctorList.get(counter);

            doctorInfo = new RelativeLayout(this.getContext());
            name = new TextView(this.getContext());
            codeID = new TextView(this.getContext());
            details = new TextView(this.getContext());

            name.setText(current.name, 0, current.name.length);
            codeID.setText(current.codeID, 0, current.codeID.length);
            details.setText(current.details, 0, current.details.length);

            doctorInfo.setBackgroundColor(current.color);

            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            doctorInfo.addView(name, params);

            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.leftMargin = 800;
            doctorInfo.addView(codeID, params);

            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = 100;
            params.leftMargin = 40;
            doctorInfo.addView(details, params);

            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.bottomMargin = 60;   //Spacing between doctors listed not working
            linearLayout.addView(doctorInfo, params);
        }
    }

    public List<DoctorView> getDoctorList() {
        //Gets list of healthcare professionals from internally saved data
        //TODO: set up method, currently returns dummy list for debuggin purposes
        List<DoctorView> list = new ArrayList();

        list.add(new DoctorView("Dr. Someone", "s0diumC0D3", "Tylenol\nStuff\nBloodwork", "#b054ff"));
        list.add(new DoctorView("Dr. Sometwo", "sdfsdf", "Stuff\nStuff\nStuff\nStuff", "#b3422f"));
        list.add(new DoctorView("Dr. Somethree", "yjjg", "Still some more stuff", "#1234ff"));
        list.add(new DoctorView("Dr. Somefour", "eryrt", "a\nb\nc", "#b0abbf"));
        list.add(new DoctorView("Dr. Somefive", "k", "stuff", "#abcdef"));
        list.add(new DoctorView("Dr. Somesix", "sdgfherrdsgdsgsf", "tab\there", "#123456"));

        return list;
    }
}