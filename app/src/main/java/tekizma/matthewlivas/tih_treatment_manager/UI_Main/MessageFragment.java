package tekizma.matthewlivas.tih_treatment_manager.UI_Main;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.net.ConnectException;

import tekizma.matthewlivas.tih_treatment_manager.R;

/**
 * Created by Nate on 3/28/2016.
 */
public class MessageFragment extends android.support.v4.app.Fragment {
    private final int MAX_LENGTH = 250; //Max character length for outgoing messages

    private View OVERVIEW = null;
    private String MESSAGE = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        OVERVIEW = inflater.inflate(R.layout.nav_choice_message, container, false);
        EditText input = (EditText) OVERVIEW.findViewById(R.id.message_input);
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count){}

            @Override
            public void afterTextChanged(Editable s) {
                /*  Changing parameter in this method, like erasing its contents
                *   will recursively call this method, so the if statement is there to prevent indfinte loop.
                */
                if (s.length() != 0) {
                    String inputText = s.toString();
                    if (inputText.contains("\n")) {
                        TextView textView = (TextView) OVERVIEW.findViewById(R.id.debugText);
                        MESSAGE = inputText;
                        textView.setText(inputText); //Debugging code
                        s.delete(0, s.length());
                    }
                }
            }
        });

        /*try {
            sendMessage(this.MESSAGE.substring(0, this.MAX_LENGTH - 1));
        } catch (ConnectException z) {

        }*/

        return OVERVIEW;
    }

    private void sendMessage(String message) throws ConnectException {
        //TODO send message to health care professional
    }
}
