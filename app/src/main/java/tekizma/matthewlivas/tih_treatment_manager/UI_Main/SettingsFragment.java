package tekizma.matthewlivas.tih_treatment_manager.UI_Main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import tekizma.matthewlivas.tih_treatment_manager.R;

/**
 * Created by Nate on 3/28/2016.
 */
public class SettingsFragment extends android.support.v4.app.Fragment {
    private View OVERVIEW = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        OVERVIEW = inflater.inflate(R.layout.nav_choice_settings, container, false);

        Switch switchOption = (Switch) OVERVIEW.findViewById(R.id.option_switch);
        switchOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView switchText = (TextView) OVERVIEW.findViewById(R.id.switch_text);
                if (((Switch) v).isChecked()) {
                    switchText.setText("the switch is on");
                } else {
                    switchText.setText("the switch is off");
                }
            }
        });

        RadioGroup radioOption = (RadioGroup) OVERVIEW.findViewById(R.id.radiobutton_option_all);
        radioOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                TextView radioText = (TextView) OVERVIEW.findViewById(R.id.radiobutton_text);

                switch (checkedId) {
                    case R.id.radiobutton_option1:
                        radioText.setText("option 1 is clicked");
                        break;
                    case R.id.radiobutton_option2:
                        radioText.setText("option 2 is clicked");
                        break;
                    case R.id.radiobutton_option3:
                        radioText.setText("option 3 is clicked");
                        break;
                }
            }
        });

        return OVERVIEW;
    }
}
