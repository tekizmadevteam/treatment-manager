package tekizma.matthewlivas.tih_treatment_manager.UI_Main;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import tekizma.matthewlivas.tih_treatment_manager.R;
import tekizma.matthewlivas.tih_treatment_manager.UI_Data.DateView;
import tekizma.matthewlivas.tih_treatment_manager.UI_Data.EventView;

/**
 * Created by Nate on 3/28/2016.
 */
public class TimelineFragment extends android.support.v4.app.Fragment {
    private View OVERVIEW = null;   //Uninflated View

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        OVERVIEW = inflater.inflate(R.layout.nav_choice_timeline, container, false);

        TextView dateArea = (TextView) OVERVIEW.findViewById(R.id.date_area);
        dateArea.setText(DateView.today());

        createTimeline();

        return OVERVIEW;
    }
    public void onViewCreated(View view, Bundle savedInstanceState) {
        String gotoDate = getArguments().getString("date");

        scrolltoDate(gotoDate);
    }

    private void createTimeline() {
        //Creates Views of timeline events and places them in scrollGroup, which is obtained using OVERVIEW
        LinearLayout scrollGroup = (LinearLayout) OVERVIEW.findViewById(R.id.scroll_group);
        LayoutParams outerParams;
        LayoutParams innerParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        innerParams.width = 200;             //Set size of event indicator.
        innerParams.height = innerParams.width;

        LinearLayout outerLayout;
        RelativeLayout innerLayout;
        TextView time;
        EventView event;
        View.OnClickListener onClickListener;

        List<EventView> timeline = getTimeline();
        int lastEvent = timeline.get(0).dateView.hour;

        for (int counter = 0; counter < timeline.size(); counter ++) {
            outerParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            outerLayout = new LinearLayout(this.getContext());
            innerLayout = new RelativeLayout(this.getContext());
            time = new TextView(this.getContext());

            outerLayout.setOrientation(LinearLayout.VERTICAL);

            event = timeline.get(counter);
            onClickListener = new View.OnClickListener() {
                public void onClick(View v) {
                    //Upon clicking an EventView, it will bring up options for having taken medication.
                    //TODO: assign uses of buttons
                    String info = ((EventView) v).info;
                    TextView infoArea = (TextView) OVERVIEW.findViewById(R.id.info_area);
                    TextView yesButton = (TextView) OVERVIEW.findViewById(R.id.button_yes);
                    TextView noButton = (TextView) OVERVIEW.findViewById(R.id.button_no);
                    TextView snoozeButton = (TextView) OVERVIEW.findViewById(R.id.button_snooze);

                    yesButton.setVisibility(View.VISIBLE);
                    noButton.setVisibility(View.VISIBLE);
                    snoozeButton.setVisibility(View.VISIBLE);

                    infoArea.setText(info.toCharArray(), 0, info.length());
                }
            };
            event.setOnClickListener(onClickListener);
            time.setText(event.dateView.getTime());

            outerParams.leftMargin = event.getDistance(lastEvent);
            lastEvent = event.dateView.hour;

            innerLayout.addView(event);
            outerLayout.addView(innerLayout, innerParams);
            outerLayout.addView(time, innerParams);
            scrollGroup.addView(outerLayout, outerParams);
        }
    }

    public void scrolltoDate(String gotoDate) {
        //TODO finish method, should scroll to coordinates of child in timeline_scroll that matches give gotoDate
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) OVERVIEW.findViewById(R.id.timeline_scroll);
        horizontalScrollView.scrollTo(-200, -200);
    }

    private List<EventView> getTimeline() {
        //Obtains list of nearing events from local memory or distant events from server
        //TODO: setup method, currently returns dummy list for debuggin purposes.

        List<EventView> list = new ArrayList<EventView>();

        list.add(new EventView(new DateView(2016, 1, 1, 9, 0), Color.parseColor("#fd86ab"), this.getContext()));
        list.get(0).info = "*\tCheck blood sugar levels";
        list.add(new EventView(new DateView(2016, 1, 1, 9, 30), Color.parseColor("#f080ab"), this.getContext()));
        list.get(1).info = "*\tTake Tylenol\n*\tTake Ibuprofen";
        list.add(new EventView(new DateView(2016, 1, 1, 11, 0), Color.parseColor("#fd8611"), this.getContext()));
        list.get(2).info = "*\tCheck blood sugar levels";
        list.add(new EventView(new DateView(2016, 1, 1, 11, 30), Color.parseColor("#f336ab"), this.getContext()));
        list.get(3).info = "IMPORTANT: Contact Dr. Jane Doe";
        list.add(new EventView(new DateView(2016, 1, 1, 12, 0), Color.parseColor("#a126ab"), this.getContext()));
        list.get(4).info = "*\tTake Tylenol";
        list.add(new EventView(new DateView(2016, 1, 1, 15, 30), Color.parseColor("#0086ab"), this.getContext()));
        list.get(5).info = "*\tGo in for bloodwork";

        return list;
    }
}
