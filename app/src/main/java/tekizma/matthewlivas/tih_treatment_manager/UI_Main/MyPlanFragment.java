package tekizma.matthewlivas.tih_treatment_manager.UI_Main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tekizma.matthewlivas.tih_treatment_manager.R;

/**
 * Created by Nate on 3/28/2016.
 */
public class MyPlanFragment extends android.support.v4.app.Fragment {
    private View OVERVIEW = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        OVERVIEW = inflater.inflate(R.layout.nav_choice_plan, container, false);

        return OVERVIEW;
    }
}
