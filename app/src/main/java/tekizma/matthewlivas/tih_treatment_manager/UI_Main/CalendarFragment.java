package tekizma.matthewlivas.tih_treatment_manager.UI_Main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;

import tekizma.matthewlivas.tih_treatment_manager.R;

/**
 * Created by Nate on 3/28/2016.
 */
public class CalendarFragment extends android.support.v4.app.Fragment {
    private View OVERVIEW = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        OVERVIEW = inflater.inflate(R.layout.nav_choice_calendar, container, false);

        TextView textView = (TextView) OVERVIEW.findViewById(R.id.calendar_button);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalendarView calendarView = (CalendarView) OVERVIEW.findViewById(R.id.calendar);
                long date = calendarView.getDate();
                //Bundle bundle

                ((MainActivity) getActivity()).replaceFragment(R.id.nav_timeline);
            }
        };
        textView.setOnClickListener(onClickListener);

        return OVERVIEW;
    }
}
