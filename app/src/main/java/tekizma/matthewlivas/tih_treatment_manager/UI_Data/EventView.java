package tekizma.matthewlivas.tih_treatment_manager.UI_Data;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by Nate on 4/13/2016.
 *
 * Used in TimelineFragment,
 * Holds information for individual medication events
 */
public class EventView extends View {
    public DateView dateView;   //Date and time
    public int color;           //Color associated with medication to colorcode nodes on timeline
    public String info;         //Details and information about event, to appear when event is clicked

    private Paint paint;

    public EventView(DateView dateView, int color, Context context) {
        super(context);
        this.dateView = dateView;
        this.color = color;
        paint = new Paint();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int x = getWidth() / 2;
        int y = getHeight() / 2;

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        canvas.drawCircle(x, y, x, paint);
        paint.setColor(color);
        canvas.drawCircle(x, y, x - 10, paint);
    }

    public int getDistance(int lastEvent) {
        //Gets distance for next event on timeline, based on how chronologically apart the events are. Minimum of 300dp.
        //Purely graphical fluff.
        int min = 150;

        if ((dateView.hour - lastEvent) > 1)
            return (dateView.hour - lastEvent) * 200;
        return min;
    }
}
