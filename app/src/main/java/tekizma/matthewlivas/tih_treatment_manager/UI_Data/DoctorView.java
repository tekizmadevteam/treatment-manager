package tekizma.matthewlivas.tih_treatment_manager.UI_Data;

import android.graphics.Color;

/**
 * Created by Nate on 4/20/2016.
 *
 * Used in AccountFragment
 * Holds information about individual healthcare professionals and what they are doing for the patient
 */
public class DoctorView {
    public char[] name,         //Name of healthcare professional
            codeID,             //Identification code for treatment plan
            details;        //List of details and appointment patient is being helped with
    public int color;        //Color assosiated with healthcare professional

    public DoctorView(String name, String codeID, String details, String color) {
        this.name = name.toCharArray();
        this.codeID = codeID.toCharArray();
        this.details = details.toCharArray();
        this.color = Color.parseColor(color);
    }
}
