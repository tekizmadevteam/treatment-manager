package tekizma.matthewlivas.tih_treatment_manager.UI_Data;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Nate on 4/8/2016.
 */
public class DateView {
    public int year, month, day, hour, minute;

    public DateView(int year, int month, int day, int hour, int minute) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }
    public String getMonth() {
        String name = "";
        switch (month) {
            case 1: name = "JAN"; break;
            case 2: name = "FEB"; break;
            case 3: name = "MAR"; break;
            case 4: name = "APR"; break;
            case 5: name = "MAY"; break;
            case 6: name = "JUN"; break;
            case 7: name = "JUL"; break;
            case 8: name = "AUG"; break;
            case 9: name = "SEP"; break;
            case 10: name = "OCT"; break;
            case 11: name = "NOV"; break;
            case 12: name = "DEC"; break;
        }

        return name;
    }
    public String getTime() {
        String time = "";

        if (hour > 12) {
            time += (hour - 12);

            if (minute < 10)
                time += ":0" + minute;
            else
                time += ":" + minute;

            time += " PM";
        } else {
            time += hour;

            if (minute < 10)
                time += ":0" + minute;
            else
                time += ":" + minute;

            time += " AM";
        }

        return time;
    }
    public long toMillis() {
        Date temp = new Date(this.year, this.month, this.day, this.hour, this.minute);

        return temp.getTime();
    }
    public String toString() {
        return getTime() + " on " + getMonth() +"/" + day + "/" + year;
    }

    public static String today() {
        //Returns today's date in DateView form.
        Calendar today = new GregorianCalendar();
        return new DateView(today.getTime().getYear() + 1900, today.getTime().getMonth() + 1, today.getTime().getDate(), today.getTime().getHours(), today.getTime().getMinutes()).toString();
    }
}
