package tekizma.matthewlivas.tih_treatment_manager;

import android.app.DownloadManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.List;

import tekizma.matthewlivas.tih_treatment_manager.Resources.UserQueries.QueryResponse;
import tekizma.matthewlivas.tih_treatment_manager.ServerInteraction.*;
import tekizma.matthewlivas.tih_treatment_manager.Resources.*;

public class TestingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);

        TextView text = (TextView) findViewById(R.id.testing_output);
        String output = "";

        MemoryManagement.loadSimpleTestServerMessages(this);

        List<Patient> patients = MemoryManagement.getSavedPatients(this);

        List<Doctor> doctors = MemoryManagement.getSavedDoctors(this);

        List<TreatmentPlan> plans = MemoryManagement.getSavedTreatmentPlans(this);

        List<TreatmentAction> actions = MemoryManagement.getSavedActionsList(this);

        TreatmentSetupQuestions questions = MemoryManagement.getSavedSetupQuestions(this);

        JSONObject error = MemoryManagement.getSavedError(this);
/*
        output = output.concat("Error:\n" + JSONTranslator.prettyPrintJSON(error) + "\n");

        output = output.concat("\nPatients\n");

        for(Patient p:patients) {
            output = output.concat(JSONTranslator.prettyPrintJSON(JSONTranslator.createJSONFromPatient(p))+"\n");
        }

        output = output.concat("\nDoctors\n");

        for(Doctor d:doctors) {
            output = output.concat(JSONTranslator.prettyPrintJSON(JSONTranslator.createJSONFromDoctor(d)) + "\n");
        }

        output = output.concat("\nTreatmentPlans\n");

        for(TreatmentPlan t: plans) {
            output = output.concat(JSONTranslator.prettyPrintJSON(JSONTranslator.createJSONFromTreatmentPlan(t)) + "\n");
        }

        output = output.concat("\nTreatmentActions\n");

        for(TreatmentAction a:actions) {
            output = output.concat(JSONTranslator.prettyPrintJSON(JSONTranslator.createJSONFromTreatmentAction(a)) + "\n");
        }
*/
        output = output.concat("\n"+ questions.getNumQuestions()+ " Questions\n");
//TODO create a way to parse a treatmentsetupquestion object into a JSON object
//TODO fix the SetupQuestions JSON Object parsting. Something is wrong, I'm only ever getting one question when I should have 4.
//        output = output.concat(questions.toString());

//        output = output.concat(MemoryManagement.readFromFileJSON(MemoryManagement.getSetupQuestionsFile(this)).toString());

        output = output.concat(JSONTranslator.prettyPrintJSON(JSONTranslator.createJSONFromTreatmentSetupQuestion(questions)));

        text.setText(output);
    }
}
